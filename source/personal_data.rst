.. _ref-personal-page:

##################################
Personal Data Structure Guidelines
##################################

.. warning::
    This document is continuous work in progress and its contents is created to disseminate the results of the data management workgroup.

Language
========

All research should be reported in English or Dutch. This also includes all documentes, folders and notes,
but also the naming of petri-dishes and/or tubes. For electronic document and folder naming, choose one language
and use it consistently.

Literature management
=====================
It is recommended to use one of the following database applications for bibliographical data:   
  * `EndNote <https://intranet.ilvo.vlaanderen.be/nl/diensten/ict/software/endnote>`_ (intranet)
  * `Mendeley <https://www.mendeley.com>`_ 
  * `Zotero <https://www.zotero.org>`_
It is advisable to choose the same application as the colleagues you will work with regularly (e.g., promotor/supervisor of PhD thesis), to allow easy exchange of literature. For more information on the differences between the applications, check this `comparison <https://libguides.wustl.edu/choose>`_

Saving files
============
Give all your files a clear name,  so that it is evident which information is stored in each file.
Take into account the following guidelines:

1. Use meaningful abbreviations.
2. Do not use generic file names that can appear in multiple folders (e.g. Results.xlx)
3. Example elements to include in a file name: data of creation, /name of experiment, project, 
   type of data, location, your initials, …).
4. For dates, use the format YYYYMMDD (e.g. 20210602) at the start of the filename.
   This makes it easier to sort files.
5. Include a two digit version number if appropriate (v01, v02, …).
6. For more tips, check the `RDMkit website <https://rdmkit.elixir-europe.org/data_organisation.html>`__

Folder structure
================

For the folder structure, you have to make a distinction with ‘work in progress’,
which will be structured in projects and 'end products' that are finalized.


::

    /
    ├── 01_own_publications
    ├── 02_phd
    │    ├── 01_doctoral_schools
    │    └── 02_thesis
    ├── 03_conferences
    ├── 04_training
    ├── 05_archived_projects
    ├── 06_active_projects
    └── 07_other

Own publications
----------------
1. Make a subfolder per publication. The folder name should clearly indicate which publication is intended 
   (keywords, journal, year). 
2. Add to this folder (once the publication is accepted) the final .docx and .pdf file,
   the reviewer comments and your answers to them, and keep figures, tables and additional data in the original
   separate files (not pdf).
3. The final data files used to write the paper (e.g files used for statistics) should also be stored in this folder.
   These data files should be clear and understandable and accompanied by  a proper readme or metadata file.
4. Not only scientific publications, but also vulgarizing publications should be kept. In this case, also keep the original
   figures. 
5. Store your final datafiles in a repository (Zenodo, Dryad, Figshare, ...). This ensures that the correct version linked to the publications will always be available.    


PhD (if applicable)
-------------------
This folder should contain:

1. A subfolder that includes your annual reports and other information (``1_doctoral_schools``)
2. Another subfolder (``2_thesis``):
    - Writing process: 1 folder per chapter – include versioning – store also remarks from supervisors.
    - 1st submitted version for the Faculty Council.
    - Submitted version for the internal defense, reports from the internal defense, comments of jury members and your answers to them.
    - Final PhD thesis in .docx and .pdf
    - PhD presentation(s)
    - Figures and Tables should also be kept in separate files.

Conferences
-----------
Create a subfolder per conference or work travel. The folder name should include the conference title and year,
so that it is clear which conference is intended. Also posters or powerpoints that you have presented,
abstracts that were submitted, books of abstracts and other interesting conference information should be kept in this folder.

Training
--------
Make a subfolder for each course, workshop or other training you follow and store all obtained information here.

Finished Projects
-----------------
Once a project is finished, you can move all relevant information that should be kept for the future to this folder; 
create a subfolder for each project. 

Active Projects
---------------
Check `this link <https://ilvo-plant-data-management.readthedocs.io/project_data.html#project-data-management-documentation>`_

 
