.. _ref-system-page
###############################################
System Specific Data Management Documentation
###############################################

.. warning::
    This document is currently work in progress and its contents was created for demonstrating the
    usage of readthedocs for structuring the results of the plant data management workgroup.

Genomics
========

High-Throughput Field Phenotyping (HTFP)
========================================

What is it about?
-----------------
 
High-Throughput Field Phenotyping (HTFP) comprises non-destructive, sensor based assessment of plant/canopy traits (above and below ground) of field experiments. HTFP captures the spatial variability present in the field, and is not lmimited to time point specific traits but also leverages dynamic traits derived from longitudinal data. Data analysis is based on traditional statistical techniques but also on machine learning and artificial intelligence.
 
Data storage
------------ 
Pre-processed HTFP data can be found per crop/experiment/project/year on the `HTFP network drive <\\clo.be\dfs\data\htfp>`_. Link this network location to your O: drive as in discussions we will always refer to the "O:" drive.  
 
If you want to work on your data make a copy of trial-data on a local drive (preferably an SSD) on your own computer. When you finish your work, clean up the unwanted files and tests, and copy it back to the right location on the O: drive. Cleaning up the data is very important, not only to keep a clear overview over all files but also to avoid unnecessary disk space usage as these files are often very large (up to 5 Gb or more!!). For example, original and pre-processed data alone for one year result in 20-25 Tb of data.  
 
If you need to perform intensive HTFP calculation tasks and related statistical analysis and modelling, you can access a dedicated windows server. After checking first with `Bart Vleminckx <bart.vleminckx@ilvo.vlaanderen.be>`_  who follows up on the server and can provide an access license (limited number available), you can access the srvhtfp which you can access through "externe verbinding" (todo Bart). Similarly, data can be copied from the `HTFP network drive <\\clo.be\dfs\data\htfp>`_ to the F: drive on the server under a main folder with your name (to keep track of whos data it is) . When you finish your analysis and copied the data back to the O: drive, please remove the folder with your data again from the F: drive to make space for new projects.
 
The server is capable of big tasks and is chosen and set-up so that users can simultaneously work on their HTPF project, however if you foresee a very big task that can hinder other users, please discuss with Bart how to solve this.
 
Standardised folder structure
-----------------------------
to do
 
Inspiration based on recent publications
----------------------------------------
 
Recent publications of HTFP research can be found in the `folder <\\clo.be\dfs\data\htfp\1_PublicationsILVO>`_. 
We are available for discussions related to your project but for the first time preferably at the time of a project proposal.
 
Contact persons (who to mention here, limited number or…)
---------------------------------------------------------

- `Peter Lootens <Peter.lootens@ilvo.vlaanderen.be>`_ (FABPS installation, drone based phenotyping)  
- `Irene Borra-Serrano <irene.borra-serrano@ilvo.vlaanderen.be>`_ (FABPS installation, drone based phenotyping)  
- `Sarah Garré <sarah.garre@ilvo.vlaanderen.be>`_ (ERT based phenotyping, soil related measurements)  
- `Tom De Swaef <tom.deswaef@ilvo.vlaanderen.be>`_ (soil related measurements, envirotyping)  
- `Bart Vleminckx <bart.vleminckx@ilvo.vlaanderen.be>`_ (HTFP server, automation)  
 


Lab management
==============

