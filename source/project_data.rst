.. _ref-project-page
#####################################
Project data management documentation
#####################################

.. warning::
    This document is currently work in progress and its contents was created for demonstrating the
    usage of readthedocs for structuring the results of the plant data management workgroup.

Data Management Plan
====================

Project data management can be guided via the design of a Data Management Plan. Consult the guidelines for designing a data management plan on `Intranet <https://intranet.ilvo.vlaanderen.be/nl>`_ . There are several templates available on `DMPOnline <https://dmponline.be>`_. The ILVO template should be used for any ILVO project and is also the template for project proposals for the Flemish government (FWO, VLAIO,...). To access the template(s):  
 - navigate your browser to `DMPOnline <https://dmponline.be>`_   
 - select ILVO as your host institution on the home page  
 - log in using your ILVO account  
 - click the `Reference` tab at the top of the page  
 - click `DMP Templates`  

During the project kick-off meeting with the scientific directors, the data management plan should be reviewed. 


Working environment (Temporary storage)
=======================================

It is very important to thoroughly think where you will be storing the data during the project. At ILVO, there are multiple options for personal and collaborative workspaces. 
The 'My Documents' folder on your PC will be synchronised to the ILVO server via folder redirection. This provides a continuous backup (if you are connected to the ILVO network or using the ILVO VPN), and allows you to work on any computer connected to the ILVO network.
To collaborate with other ILVO colleagues on the same project, the ILVO IT department can set up a dedicated project network drive. These drives are, however, not accessible for external project partners.  

Alternatively, the Microsoft 365 software provides more options like OneDrive, Sharepoint and Teams. These different possibilities may seem a bit overwhelming, but, in short:  
 * Sharepoint is the backbone of the system on which all data are stored. You can make shared libraries for specific projects, where all project partners can get access to.   
 * Teams is an interface to the shared libraries that are on the Sharepoint drive. It allows you to make shared libraries (or 'Teams'), invite project collaborators and communicate with them.  
 * OneDrive is your personal space (1 TB) on the ILVO Sharepoint drive to use as your personal working directory and Teams is an interface to access collaborative projects  

Interesting tutorials on deciding which working environment to use (local PC, or using Onedrive or Sharepoint) are available:  
 * `When to use which tool: SharePoint, OneDrive, or Microsoft Teams <https://www.youtube.com/watch?v=5Mzb81zzFH0>`_  
 * `How to sync OneDrive, SharePoint, and Microsoft Teams files to computer or smart phone <https://www.youtube.com/watch?v=4lJXVIIwGHY>`_

Hands-on guidelines to properly install and use are available for: 
 * `OneDrive <https://ilvo.sharepoint.com/:b:/s/Data-Plant/EYAtbKEg47dOpk08lmY6FCABgKw1zGleyKPxyGNE2B4LgA?e=Cf5qk1>`_ 
 * `OneNote and Sharepoint <https://ilvo.sharepoint.com/:b:/s/Data-Plant/EcQuinn9RuRNgCcDvLU0bs0BlO-CCsOT9x9hPFhBetZghQ?e=Ye8hmI>`_ 


Folder structure
================
You have to create a folder for each running project you are involved in. Ideally, you describe the contents of this folder in your `data management plan <https://ilvo-plant-data-management.readthedocs.io/project_data.html#data-management-plan>`_.
Always add a number in front of the folder name, so folders are always sorted in the same way.
The subfolders that are advised will depend on the project, but here, we provide some inspiration:

1. Subfolder per work package, with next level subfolders per experiment (or group of experiments):
    - Keep an overview file (metadata file or readme file) for each experiment. This file should describe the materials and methods in detail, list contact details from partner institutes or from persons that you have obtained material (plant material, fungal isolates, …) from, and list main conclusions. When your experiments include pictures, DNA gels, sequencing data, tables or other relevant files, list where these files are stored so that they can be retrieved later. If applicable, make a reference to the notebooks (`OneNote <https://ilvo.sharepoint.com/sites/Data-Plant/Gedeelde%20documenten/Forms/AllItems.aspx?id=%2Fsites%2FData%2DPlant%2FGedeelde%20documenten%2FGeneral%2F99%5FOpenForUse%2F20220422%5FOneNote%5FTutorial%2Epdf&parent=%2Fsites%2FData%2DPlant%2FGedeelde%20documenten%2FGeneral%2F99%5FOpenForUse&p=true&ga=1>`_) of the laboratory staff that assisted you.
    - For small experiments that include e.g. only 1 excel file, the overview can be given in the first tab sheet of the excel file.
    - For data processing, use the tab sheets in Excel (as many as needed) and give them a clear name.
    - Do not only keep data from trials that went well, also keep data from failed trials or exploratory trials. Always add a conclusion to these files to describe the lessons learned. These trials may be of interest for researchers working on the same topic after you. 
2. Subfolder for project administration.
3. Subfolder for project meetings (powerpoint presentations, meeting reports, …).
4. Subfolder for project output. This will be a working folder, finalised papers, presentations for conferences,… 
   can be moved to the appropriate general folder.
5. Subfolder Data Management Plan 


Designing a Tidy data set
=========================   
Very often, a lot of effort is required to get data into a good shape ready for analysis. After Wickham (2014), we call well-structured data tidy data. Tidy datasets are easy to manipulate, model and visualise. So there is a high return on investment.

The tidy data framework offers a coherent rationale to organise the data. It is based on three simple principles only: 
 #. each variable is a column,  
 #. each observation is a row, and  
 #. each type of observational unit is a table. 

The framework makes it easy to tidy messy datasets. Yet, some exercise is necessary to master the principles. Inspired on Wickham (2014), the `presentation <https://ilvo.sharepoint.com/:p:/s/Data-Plant/ERzLWx5j_4RDloiLEOm5_kIB3wBoFWkiOf7OT5Ac1NkiFg?e=ruZBvO>`_ illustrates the main ideas. What can go wrong and how to solve it.
You can find examples of tidy data sets `here <https://ilvo.sharepoint.com/:f:/s/Data-Plant/Evguxcaw7JpKtzRBeFyYp9sBtj30jShAjL0TO4n7M-gDvQ?e=PIaoTf>`_.


Wickham, H. 2014 Tidy data. Journal of Statistical Software, 59 (10), 23.


Archiving projects (Permanent storage)
======================================

Data from finished projects (link to Personal data) should be archived on a permanent drive at ILVO, and whenever possible it is encouraged to archive the data on a publicly accessible generic (e.g., `Zenodo <https://zenodo.org>`_, `figshare <https://figshare.org>`_, ...) or a field-specific repository (check out this `overview <https://www.nature.com/sdata/policies/repositories#general>`_).


