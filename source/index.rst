.. ILVO Plant Data Management documentation master file, created by
   sphinx-quickstart on Thu Jun  3 13:11:28 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

######################################################
Welcome to ILVO Plant Data Management's documentation!
######################################################

This website is an initiative of the ILVO-Plant data management working group to provide guidelines, supporting information and (links to) tools to help you improve your data management. This will benefit your personal efficiency, but also the re-use of your data, protocols by colleagues within and outside of ILVO. The content of this website should support you to apply the `FAIR principles (intranet) <https://intranet.ilvo.vlaanderen.be/nl/diensten/onderzoeksco%C3%B6rdinatie/data-en-informatiebeheer/wat-is-fair-data>`_ in your daily routine, thereby structuring the organisation at the level of the `person <personal_data.html>`__ (who is involved in multiple projects), as well as at the level of the individual `project <project_data.html>`__. Finally, ILVO-Plant has several `'bigger' systems <system_specific_data.html>`__ that produce systematic data, for which dedicated data workflows are developed.
For specific questions on data management, feel free to contact Elien Dewitte, the ILVO Data manager at data@ilvo.vlaanderen.be. 
Enjoy!!



.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Contents:

   personal_data
   project_data
   system_specific_data
